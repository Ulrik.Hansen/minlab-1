package INF102.lab1.triplicate;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    /*
    @Override
    public T findTriplicate(List<T> list) {
        HashMap<T,Integer> count = new HashMap<T,Integer>();
        for(T i : list) {
            if (count.get(i) == null) {
                count.put(i,1);
                continue;
            }
            count.put(i,(count.get(i)+1));
            if (count.get(i)==3) {
                return i;
            }
        }
        return null;
    }
    */
    
    @Override
    public T findTriplicate(List<T> list) {
        HashSet<T> set1 = new HashSet<T>();
        HashSet<T> set2 = new HashSet<T>();
        for(T i : list) {
            if (set1.contains(i)) {
                if (set2.contains(i)) {
                    return i;
                } else {
                    set2.add(i);
                }
            } else {
                set1.add(i);
            }
        }

        return null;
    }
    
}
